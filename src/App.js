import React, { useEffect, useState } from 'react';
import ListItem from './components/list-item';
import Header from './components/header';
import ItemDetails from './components/item-details';
import {API_KEY} from "./constHelper";
import {
    InputGroup,
    FormControl,
    Button,
    Container,
    Row,
    Col,
    Navbar,
    Card,
    Alert,
    Modal,
} from 'react-bootstrap';
import {
    Spin
} from 'antd';

import 'antd/dist/antd.css';


const Footer = ({ footerText }) => {
    return (
        <Navbar bg="light" expand="mb">
            <footer style={{ textAlign: 'center' }}>{footerText}</footer>
        </Navbar>
    )
}








const Loader = () => (
    <div style={{ margin: '20px 0', textAlign: 'center' }}>
        <Spin />
    </div>
)

function App() {

    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [query, setQuery] = useState('avengers');
    const [activateModal, setActivateModal] = useState(false);
    const [detail, setShowDetail] = useState(false);
    const [detailRequest, setDetailRequest] = useState(false);


    useEffect(() => {

        setLoading(true);
        setError(null);
        setData(null);

        fetch(`https://www.omdbapi.com/?apikey=${API_KEY}&s=${query}`)
            .then(resp => resp)
            .then(resp => resp.json())
            .then(response => {
                if (response.Response === 'False') {
                    setError(response.Error);
                }
                else {
                    setData(response.Search);
                }

                setLoading(false);
            })
            .catch(({ message }) => {
                setError(message);
                setLoading(false);
            })

    }, [query]);


    return (
        <div className="App">
            <Container>

                <Header titleText="Find Movie Data" searchHandler={setQuery} />
                <Container>
                    {loading && <Loader />}
                    {error !== null &&
                        <Alert variant="danger" className="col-2 offset-5">Sorry, item not found.</Alert>
                    }
                    <Row>
                        {data !== null && data.length > 0 && data.map((result, index) => (
                            <Col xs="3">

                                <ListItem
                                    ShowDetail={setShowDetail}
                                    DetailRequest={setDetailRequest}
                                    ActivateModal={setActivateModal}
                                    key={index}
                                    {...result}
                                />
                            </Col>
                        ))}
                    </Row>

                    <Modal
                        show={activateModal}
                        onHide={() => setActivateModal(false)}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Details</Modal.Title>
                        </Modal.Header>
                        {detailRequest === false ?
                            (<ItemDetails {...detail} />) :
                            (<Loader />)
                        }
                    </Modal>
                </Container>
                <Footer footerText="Kamila & Erbol ©2020" />
            </Container>
        </div>
    );
}

export default App;
