import {Button, Container, FormControl, InputGroup, Navbar} from "react-bootstrap";
import React, {Component} from "react";

export default class Header extends Component {
    state = {
        query: ''
    };

    onChange = (e) => {
        this.setState({query : e.target.value});
    };

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }


    render(props)
    {
        return (
            <Container fluid>
                <Navbar bg="light" expand="mb">
                    <Navbar.Brand href="#home">{this.props.titleText}</Navbar.Brand>

                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Enter movie, series, episode"
                            aria-label="Enter movie, series, episode name"
                            aria-describedby="basic-addon2"
                            onChange={this.onChange}
                            value={this.state.query}
                        />
                        <Button
                            onClick={e => this.props.searchHandler(this.state.query)}
                            variant="primary"
                        >search</Button>
                    </InputGroup>
                </Navbar>
            </Container>
        )
    }
};