import {Card, Col, Row} from "react-bootstrap";
import React from "react";

export const ItemDetails = ({ Title, Poster, imdbRating, Rated, Runtime, Genre, Plot }) => {
    return (
        <Card>
            <Row>
                <Col>
                    <Card.Img src={Poster === 'N/A' ? 'https://placehold.it/198x264&text=Image+Not+Found' : Poster} />
                </Col>
                <Col>

                    <Card.Body>
                        <Card.Title>{Title}</Card.Title>
                        <Card.Text>
                            <p><strong>Rated</strong> {Rated}</p>
                            <p><strong>Rating</strong> {imdbRating}/10</p>
                            <p><strong>Genre</strong> {Genre}</p>
                            <p><strong>Runtime</strong> {Runtime}</p>
                            <p><strong>Plot</strong> {Plot}</p>
                        </Card.Text>
                    </Card.Body>
                </Col>
            </Row>
        </Card>
    )
}