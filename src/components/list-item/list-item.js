import {Button, Card} from "react-bootstrap";
import  {API_KEY} from "../../constHelper";
import React from "react";

export const ListItem = ({ Title, Year, imdbID, Poster, Type, ShowDetail, DetailRequest, ActivateModal }) => {

    const clickHandler = () => {
        ActivateModal(true);
        DetailRequest(true);

        fetch(`http://www.omdbapi.com/?i=${imdbID}&apikey=${API_KEY}`)
            .then(resp => resp)
            .then(resp => resp.json())
            .then(response => {
                DetailRequest(false);
                ShowDetail(response);
            })
            .catch(({ message }) => {
                DetailRequest(false);
            })
    }

    return (
        <Card className="card_style" style={{ flex: 1 }}>
            <Card.Img variant="top" src={Poster === 'N/A' ? 'https://placehold.it/198x264&text=Image+Not+Found' : Poster} />
            <Card.Body>
                <Card.Title>{Title}</Card.Title>
                <Button onClick={() => clickHandler()}>Details</Button>
            </Card.Body>
            <Card.Footer>
                <small className="text-muted">{Year}, {Type}</small>
            </Card.Footer>
        </Card>
    )
}